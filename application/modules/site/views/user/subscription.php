<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <?php
      if(!empty($data)) {
        ?>
        <div class="col-12 col-sm-12 d-flex align-items-stretch">
          <div class="card card-primary w-100">
            <div class="card-header">
              <h3 class="card-title font-weight-normal">Silakan pilih salah satu paket dibawah ini:</h3>
            </div>
            <div class="card-body p-0">
              <table class="table table-hover" width="100%">
                <tbody>
                  <?php
                  foreach($data as $pkg) {
                    $txt = urlencode("Saya ingin mendapatkan akses modul belajar *".strtoupper($pkg[COL_KATEGORI])." - ".$pkg[COL_SUBSDUR]." ".ucwords(getEnumPeriod($pkg[COL_SUBSTERM]))."* di ".$this->setting_web_desc."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
                    ?>
                    <tr>
                      <td class="font-weight-bold"><?=$pkg[COL_KATEGORI]?></td>
                      <td class="font-italic"><?=$pkg[COL_SUBSREMARKS]?></td>
                      <td class="text-right" style="width: 10px; white-space: nowrap"><?=$pkg[COL_SUBSDUR]?></td>
                      <td><?=ucwords(getEnumPeriod($pkg[COL_SUBSTERM]))?></td>
                      <td class="text-right" style="width: 10px; white-space: nowrap">Rp. <?=number_format($pkg[COL_SUBSPRICE])?>,-</td>
                      <td style="width: 10px; white-space: nowrap">
                        <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=<?=$txt?>" target="_blank" class="btn btn-sm btn-outline-primary">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                      </td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
        <?php
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                Maaf, belum ada data tersedia saat ini.
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
