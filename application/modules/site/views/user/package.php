<?php
$ruser = GetLoggedUser();
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row align-items-stretch">
      <?php
      if(!empty($data)) {
        foreach($data as $dat) {
          $rpkgs = $this->db->select('mtestpackage.*, mkategori.Kategori')
          ->join(TBL_MKATEGORI,TBL_MKATEGORI.'.'.COL_UNIQ." = ".TBL_MTESTPACKAGE.".".COL_IDKATEGORI,"left")
          ->where((!empty($dat[COL_UNIQ])?'mtestpackage.IdKategori='.$dat[COL_UNIQ]:'mtestpackage.IdKategori is null'))
          ->order_by(COL_PKGISACTIVE, 'desc')
          ->order_by('mkategori.Kategori', 'asc')
          ->order_by(COL_PKGNAME, 'asc')
          ->get(TBL_MTESTPACKAGE)
          ->result_array();
          ?>
          <div class="col-12 col-sm-6 d-flex align-items-stretch">
            <div class="card card-outline card-primary w-100">
              <div class="card-header">
                <h3 class="card-title font-weight-bold"><?=$dat[COL_KATEGORI]?></h3>
              </div>
              <div class="card-body p-0">
                <table class="table table-hover" width="100%">
                  <tbody>
                    <?php
                    $n=0;
                    foreach($rpkgs as $pkg) {
                      $txt = urlencode("Saya ingin mendapatkan akses simulasi *".strtoupper($pkg[COL_PKGNAME])."* di ".$this->setting_org_name."\n\nBerikut info akun saya:\nNama: *".$ruser[COL_FULLNAME]."*\nUsername: *".$ruser[COL_USERNAME]."*");
                      ?>
                      <tr <?=$n>=4?'class="d-none"':''?>>
                        <td><?=$pkg[COL_PKGNAME]?><?=!empty($pkg[COL_PKGDESC])?'<br /><small class="font-italic">'.$pkg[COL_PKGDESC].'</small>':''?></td>
                        <td class="text-right">Rp. <?=number_format($pkg[COL_PKGPRICE])?></td>
                        <td style="width: 10px; white-space: nowrap">
                          <a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_phone?>&text=<?=$txt?>" target="_blank" class="btn btn-sm btn-outline-primary">PILIH&nbsp;<i class="far fa-arrow-circle-right"></i></a>
                        </td>
                      </tr>
                      <?php
                      $n++;
                    }
                    if(count($rpkgs)>4) {
                      ?>
                      <tr>
                        <td colspan="3" class="text-center font-italic"><a href="#" class="btn-pkg-collapse">LIHAT SEMUA (<?=count($rpkgs)?>)</a></td>
                      </tr>
                      <?php
                    }
                    ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
          <?php
        }
      } else {
        ?>
        <div class="col-md-12">
          <div class="card">
            <div class="card-body">
              <p class="text-center mb-0 font-italic">
                Maaf, belum ada data tersedia saat ini.
              </p>
            </div>
          </div>
        </div>
        <?php
      }
      ?>
    </div>
  </div>
</div>
<script>
$(document).ready(function(){
  $('.btn-pkg-collapse').click(function(){
    var tbl = $(this).closest('table');
    $('tr.d-none', tbl).removeClass('d-none');
    $(this).closest('tr').addClass('d-none')
    return false;
  });
});
</script>
