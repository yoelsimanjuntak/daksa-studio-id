<style>
#datalist_filter {
  text-align: left !important;
  display: inline-block !important;
}
#datalist_filter label {
  font-weight: 700;
}
</style>
<?php
$arrsym = array_diff(scandir(MY_IMAGEPATH.'symbols/'), array('.', '..'));
?>
<div class="content-header">
  <div class="container-fluid">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h3 class="m-0 text-dark font-weight-light"><?=strtoupper($title)?></h3>
      </div>
      <div class="col-sm-6 float-sm-right">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="<?=site_url()?>">Dashboard</a></li>
          <li class="breadcrumb-item active"><?=$title?></li>
        </ol>
      </div>
    </div>
  </div>
</div>
<section class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-12">
        <div class="card card-default">
          <div class="card-header">
            <div class="card-tools text-center" style="float: none !important">
              <a href="<?=site_url('site/master/symbol-add')?>" type="button" class="btn btn-tool btn-add text-primary"><i class="fas fa-plus"></i>&nbsp;TAMBAH</a>
            </div>
          </div>
          <div class="card-body">
            <div class="row">
              <?php
              foreach ($arrsym as $s) {
                $a = mime_content_type(MY_IMAGEPATH.'symbols/'.$s);
                $image_type = $a;
                if (!in_array($image_type , array('image/gif' , 'image/jpeg' ,'image/png' , 'image/jpg')))
              	{
                  continue;
              	}
                ?>
                <div class="col-sm-1 mb-3" style="min-width: 25px">
                  <div class="btn-group-vertical">
                    <button type="button" class="btn btn-block btn-opt btn-outline-secondary text-center" data-toggle="tooltip" data-placement="top" data-title="<?=$s?>">
                      <img src="<?=MY_IMAGEURL.'symbols/'.$s?>" style="width:50px; height: 50px" />
                    </button>
                    <button href="button" data-url="<?=site_url('site/master/symbol-delete/'.$s)?>" class="btn btn-block btn-xs btn-del-data btn-danger text-center">
                      <i class="far fa-times"></i>
                    </button>
                  </div>
                </div>
                <?php
              }
              ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="modal-form" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <span class="modal-title">TAMBAH </span>
      </div>
      <form id="form-symbol" action="" method="post">
        <div class="modal-body">
          <div class="form-group mb-0">
            <label>FILE</label>
            <div class="custom-file">
              <input type="file" class="custom-file-input" name="userfile" accept="image/png">
              <label class="custom-file-label" for="userfile">Pilih File</label>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-sm btn-outline-danger" data-dismiss="modal"><i class="far fa-times-circle"></i>&nbsp;BATAL</button>
          <button type="submit" class="btn btn-sm btn-outline-success"><i class="far fa-check-circle"></i>&nbsp;SIMPAN</button>
        </div>
      </form>
    </div>
  </div>
</div>
<script type="text/javascript">
$(document).ready(function() {
  bsCustomFileInput.init();
  $('[data-toggle="tooltip"]').tooltip();
  $('.btn-add').click(function(){
    var a = $(this);
    var editor = $("#modal-form");

    editor.modal("show");
    $("button[type=submit]", editor).unbind('click').click(function() {
      var dis = $(this);
      dis.html("Loading...").attr("disabled", true);
      $('#form-symbol').ajaxSubmit({
        dataType: 'json',
        url : a.attr('href'),
        success : function(data){
          if (data.error==0) {
            location.reload();
          } else {
            toastr.error(data.error);
          }
        },
        complete: function(data) {
          dis.html('<i class="far fa-check-circle"></i>&nbsp;SIMPAN').attr("disabled", false);
          $('input', editor).val('');
          editor.modal("hide");
        }
      });
    });
    return false;
  });

  $('.btn-add-data').click(function() {
    $('#modal-add').modal('show');
    return false;
  });
  $('.btn-del-data').click(function() {
    var url = $(this).data('url');
    if(url) {
      if(confirm('Apakah anda yakin?')) {
        location.href = url;
      }
    }
  });
});
</script>
