<?php
$numStatToday = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m-%d")=', date('Y-m-d'))->count_all_results(TBL_WEBLOGS);
$numStatMonthly = $this->db->where('DATE_FORMAT(Timestamp, "%Y-%m")=', date('Y-m'))->count_all_results(TBL_WEBLOGS);
$numStatTotal = $this->db->count_all_results(TBL_WEBLOGS);

$rwelcome = $this->db
->where(COL_CONTENTTYPE,'WelcomeText')
->get(TBL_WEBCONTENT)
->row_array();

$rtestimoni = $this->db
->where(COL_CONTENTTYPE,'Testimonial')
->order_by(COL_UNIQ, 'desc')
->limit(12)
->get(TBL_WEBCONTENT)
->result_array();

$rgaleri = $this->db
->where(COL_CONTENTTYPE,'Galeri')
->order_by(COL_UNIQ, 'desc')
->limit(12)
->get(TBL_WEBCONTENT)
->result_array();

$qresult = @"
select sess.Uniq, UPPER(u.Fullname) as Fullname, sum(sheet.QuestScore) as Score
from tsession sess
left join tsessiontest test on test.IDSession=sess.Uniq
left join tsessionsheet sheet on sheet.IDTest = test.Uniq
left join users u on u.Username=sess.Username
group by sess.Uniq,u.Fullname
order by sum(sheet.QuestScore) desc
";
$rresult = $this->db->query($qresult)->result_array();
?>
<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="<?=$this->setting_web_desc?>">
  <meta name="author" content="Partopi Tao">
  <meta name="keyword" content="daksa, studio, daksa studio, course, partopi tao, psikotest, bimbel, psikotest online, bimbel online, cat">
  <meta property="og:title" content="<?=$this->setting_web_name?>" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?=base_url()?>" />
	<meta property="og:image" content="<?=MY_IMAGEURL.'logo-secondary.png'?>" />
  <meta property="og:image:width" content="200" />
  <meta property="og:image:height" content="200" />

  <title><?=$this->setting_web_name.' - '.$this->setting_web_desc?></title>

  <link href="<?=base_url()?>assets/themes/gotto/css/fonts.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.carousel.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/owl.theme.default.min.css" rel="stylesheet">
  <link href="<?=base_url()?>assets/themes/gotto/css/tooplate-gotto-job.css" rel="stylesheet">

  <link rel="stylesheet" href="<?=base_url()?>assets/tbs/fontawesome-pro/web/css/all.min.css" />

  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.min.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/bootstrap.min.js"></script>

  <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>-->
  <script src="<?=base_url()?>assets/themes/gotto/js/jquery.modal.js"></script>
  <link href="<?=base_url()?>assets/themes/gotto/css/jquery.modal.css" rel="stylesheet">
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />-->

  <link rel="icon" type="image/png" href=<?=base_url().$this->setting_web_icon?>>
  <style>
  .se-pre-con {
    position: fixed;
    left: 0px;
    top: 0px;
    width: 100%;
    height: 100%;
    z-index: 9999;
    background: url('<?=base_url().$this->setting_web_preloader?>') center no-repeat #fff;
  }
  .categories-block:hover {
    border-color: var(--secondary-color) !important;
  }
  .btn-float{
  	position:fixed;
  	width:60px;
  	height:60px;
  	bottom:100px;
  	right:40px;
  	background-color:#25d366;
  	color:#FFF;
  	border-radius:50px;
  	text-align:center;
    font-size:30px;
  	box-shadow: 2px 2px 3px #999;
    z-index:100;
  }

  .my-float{
  	margin-top:16px;
  }

  a {
    .job-image-box-wrap {
      position: relative;
      width:100px;
      height:100px;
      background-size:cover;
         &:before {
            opacity:0;
            content: '';
            position: absolute;
            top:0px;
            right:0px;
            left:0px;
            bottom:0px;
            background-color: #717275;
            mix-blend-mode:multiply;
            transition:opacity .25s ease;
         }
    } &:hover {
       .job-image-box-wrap {
          &:before {
             opacity:1;
          }
       }
    }
  }

  .modal {
    height: auto !important;
    overflow: visible !important;
  }
  .owl-carousel .owl-item .item-hero {
    min-height: 200px !important;
  }
  .owl-carousel .owl-item {
    /*max-width: 100% !important;*/
  }
  @media screen and (min-width: 992px) {
    .owl-carousel .owl-item .item-hero {
      height: 360px !important;
    }
  }
  </style>
</head>
<body id="top">
  <div class="se-pre-con"></div>
  <nav class="navbar navbar-expand-lg">
    <div class="container">
      <a class="navbar-brand d-flex align-items-center" href="<?=site_url()?>">
        <img src="<?=base_url().$this->setting_web_logo2?>" class="img-fluid logo-image" style="width: 240px !important">
      </a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav align-items-center ms-lg-5">
              <li class="nav-item ms-lg-auto">
                  <a class="nav-link active" href="<?=site_url()?>">Beranda</a>
              </li>
              <!--<li class="nav-item">
                <a class="nav-link" href="#article">Artikel</a>
              </li>-->
              <!--<li class="nav-item">
                <a class="nav-link" href="#galeri">Galeri</a>
              </li>-->
              <li class="nav-item">
                <a class="nav-link" href="#testimonial">Testimoni</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="https://lulusmurni.com/">LULUSMURNI.COM</a>
              </li>
              <!--<li class="nav-item">
                <a class="nav-link custom-btn btn" href="<?=site_url('site/user/register')?>"><i class="far fa-user-plus"></i> Daftar</a>
              </li>-->
          </ul>
      </div>
    </div>
  </nav>
  <main>
    <header class="site-header" style="background-image: url('<?=MY_IMAGEURL.'img-bg-overlay2.png'?>') !important">
      <div class="section-overlay"></div>
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-12 text-center">
            <h2 class="text-white"><?=strtoupper($title)?></h2>
          </div>
        </div>
      </div>
    </header>
    <section class="reviews-section section-padding">
      <div class="container">
        <div class="row">
          <div class="col-12 mb-2">
            <div class="reviews-thumb" style="padding: 20px !important">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>NAMA</th>
                    <th style="text-align: center">SKOR</th>
                    <th>TOTAL</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no=1;
                  foreach($rresult as $r) {
                    $arrdet = array();
                    $idSession=$r[COL_UNIQ];
                    $qdet = @"
                    select
                    	test.TestName,
                    	sheet.QuestGroup,
                    	sum(sheet.QuestScore) as Score
                    from tsessiontest test
                    left join tsessionsheet sheet on sheet.IDTest = test.Uniq
                    where test.IDSession=$idSession
                    group by test.TestName, sheet.QuestGroup
                    ";
                    $rdet = $this->db->query($qdet)->result_array();
                    foreach($rdet as $d) {
                      $arrdet[] = (!empty($d[COL_QUESTGROUP])?$d[COL_QUESTGROUP]:$d[COL_TESTNAME]).' = '.number_format($d['Score']);
                    }
                    ?>
                    <tr>
                      <td style="width: 10px; text-align: right; white-space: nowrap"><?=$no?>.</td>
                      <td><?=$r[COL_FULLNAME]?></td>
                      <td style="text-align: center; white-space: nowrap">
                        <?php
                        if(!empty($arrdet)) {
                          echo implode(" | ",$arrdet);
                        } else {
                          echo '-';
                        }
                        ?>
                      </td>
                      <td style="width: 10px; text-align: right; white-space: nowrap"><?=number_format($r['Score'])?></td>
                    </tr>
                    <?php
                    $no++;
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </section>
  </main>

  <footer class="site-footer" id="kontak">
    <div class="container">
      <div class="row">
        <div class="col-lg-7 col-md-6 col-12 mb-3">
          <div class="d-flex align-items-center mb-4">
            <img src="<?=MY_IMAGEURL.'logo-full.png'?>" class="img-fluid logo-image" style="width: 300px !important">
          </div>
          <p class="mb-2">
            <i class="custom-icon fas fa-map-marked-alt me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_address?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-phone-rotary me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_phone?></a>
          </p>

          <p class="mb-2">
            <i class="custom-icon fas fa-envelope me-1"></i>
            <a href="#" class="site-footer-link"><?=$this->setting_org_mail?></a>
          </p>
        </div>
        <div class="col-lg-5 col-md-6 col-12 mt-3 mt-lg-0">
          <h6 class="site-footer-title">Statistik Pengunjung</h6>
          <div class="newsletter-form">
            <p class="mb-0"><small>HARI INI</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatToday)?></strong></p>
            <p class="mb-0"><small>BULAN INI</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatMonthly)?></strong></p>
            <p class="mb-0"><small>TOTAL</small><strong style="float: right !important; font-size: .875em;"><?=number_format($numStatTotal)?></strong></p>
          </div>
        </div>
      </div>
    </div>

    <div class="site-footer-bottom">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <p class="copyright-text" style="margin-right: 0 !important">&copy; <?=date('Y')?> <?=$this->setting_web_name?><span style="float: right">By : <a class="sponsored-link" rel="sponsored" href="https://www.linkedin.com/in/yoelrolas/" target="_blank">Partopi Tao</a></span></p>
          </div>
          <!--<a class="back-top-icon bi-arrow-up smoothscroll d-flex justify-content-center align-items-center" href="#top"></a>-->
        </div>
      </div>
    </div>
  </footer>
  <!--<a href="https://api.whatsapp.com/send?phone=<?=$this->setting_org_fax?>&text=Halo" class="btn-float" target="_blank">
    <i class="fab fa-whatsapp my-float"></i>
  </a>-->
  <div id="modal-galeri" class="modal">
    <h5 style="color: var(--primary-color)">Loading...</h5>
    <img style="width: 100%;" src="" />
    <p class="mt-2 text-center">-</p>
  </div>
  <script src="<?=base_url()?>assets/themes/gotto/js/counter.js"></script>
  <script src="<?=base_url()?>assets/themes/gotto/js/custom.js"></script>
  <script type="text/javascript">
  $(document).ready(function(){
    $(".se-pre-con").fadeOut("slow");
  });
  </script>
</body>
</html>
